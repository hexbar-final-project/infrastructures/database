/* Inserting */

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('CitizenKane', 'OrsonWelles', 'OrsonWelles', '6', '1941', '0', '0', 'Drama');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('The Godfather', 'Albert Stotland Ruddy', 'Francis Ford Coppola', '2', '1972', '0', '0', 'Crime');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('Schindlers List', 'Steven Spielberg', 'Steven Spielberg', '1', '1993', '0', '0', 'Drama');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('Star Wars', 'Gary Kurtz', 'George Lucas', '10', '1977', '0', '0', 'Science Fiction');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('The Avengers', 'Kevin Feige', 'Joss Whedon', '65', '2012', '0', '0', 'Superhero');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('American Pie', 'Warren Zide', 'Paul Weitz', '12', '1999', '0', '0', 'Comedy');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('Forest Gump', 'Wendy Finerman', 'Robert Zemeckis', '0', '1994', '0', '0', 'Comedy Drama');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('Bonnie And Clyde', 'Warren Beatty', 'Arthur Penn', '2', '1967', '0', '0', 'Crime');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('Rocky', 'Irwin Winkler', 'John G. Avildsen', '5', '1976', '0', '0', 'Drama');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('Spider Man', 'Laura Ziskin', 'Sam Raimi', '6', '2002', '0', '0', 'Superhero');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('Venom', 'Avi Arad', 'Ruben Fleischer', '6', '2018', '0', '0', 'Superhero');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('Deadpool', 'Ryan Reynolds', 'Tim Miller', '7', '2016', '0', '0', 'Superhero');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('Dont Mess With The Zohan', 'Adam Sandler', 'Dennis Dugan', '12', '2008', '0', '0', 'Action Comedy');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('Jaws', 'Richard D. Zanuck', 'Steven Spielberg', '0', '1975', '0', '0', 'Thriller');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('Fast & Furious', 'Neal H. Moritz', 'Rob Cohen', '1', '2001', '0', '0', 'Action');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('Eternals', 'Kevin Feige', 'Chloé Zhao', '0', '2021', '0', '0', 'Superhero');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('Black Widow', 'Kevin Feige', 'Cate Shortland', '4', '2021', '0', '0', 'Superhero');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('Doctor Strange', 'Kevin Feige', 'Scott Derrickson', '5', '2016', '0', '0', 'Superhero');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('Dune', 'Mary Parent', 'Denis Villeneuve', '1', '2021', '0', '0', 'Science Fiction');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('Murder Mystery', 'Adam Sandler', 'Kyle Newacheck', '5', '2019', '0', '0', 'Comedy Mystery');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('Red Notice', 'Beau Flynn', 'Rawson Marshall Thurber', '7', '2021', '0', '0', 'Action Comedy');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('Free Guy', 'Ryan Reynolds', 'Shawn Levy', '7', '2021', '0', '0', 'Science Fiction Comedy');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('The Proposal', 'David Hoberman', 'Anne Fletcher', '4', '2009', '0', '0', 'Romantic Comedy');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('Green Lantern', 'Donald De Line', 'Martin Campbell', '5', '2011', '0', '0', 'Superhero');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('The Adam Project', 'David Ellison', 'Shawn Levy', '1', '2022', '0', '0', 'Science Fiction Action Comedy');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('6 Underground', 'Ian Bryce', 'Michael Bay', '1', '2019', '0', '0', 'Vigilante Action Thriller');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('The Hitmans Bodyguard', 'Ryan Reynolds', 'Patrick Hughes', '55', '2017', '0', '0', 'Action Comedy');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('Bullet Train', 'Kelly McCormick', 'David Leitch', '7', '2022', '0', '0', 'Action Comedy');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('Just Friends', 'Chris Bender', 'Roger Kumble', '1', '2005', '0', '0', 'Romantic Comedy');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('The Change UP', 'David Dobkin', 'David Dobkin', '3', '2011', '0', '0', 'Romantic Comedy');

INSERT INTO films (F_Name, F_Producer, F_Director, F_Prizes, F_Year, F_Rating, F_Count_Rating, F_Type) 
VALUES ('R.I.P.D.', 'Neal H. Moritz', 'Robert Schwentke', '5', '2013', '0', '0', 'Science Fiction Action Comedy');

/* 30 movies */

INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('James', 'Anderson', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('William', 'Roberts', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('John', 'Mitchell', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Michael', 'Jhonson', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Robert', 'Lester', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('David', 'Williams', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Joseph', 'Martinez', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Mike', 'Davis', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Daniel', 'Smith', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Matthew', 'Lee', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Andrew', 'Wayne', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Harry', 'Wilson', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Justin', 'Hall', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Barry', 'Scott', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Ethan', 'White', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Jack', 'Robinson', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Ryan', 'Turner', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Rob', 'Banks', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Jason', 'Young', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Brian', 'Green', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Mark', 'Nelson', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Nathan', 'Carter', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Bruce', 'Lee', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Bill', 'Bord', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Montgomery', 'Burns', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Kevin', 'Harris', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Davis', 'Clark', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Benjamin', 'Cohen', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Jacob', 'Moore', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Brandon', 'Rivers', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Oliver', 'Robinson', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Samuel', 'Turner', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Oliver', 'Robinson', 'Male');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Caleb', 'Jhonson', 'Male');




INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Emma', 'Jhonson', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Mia', 'Smith', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Ava', 'Williams', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Sophia', 'Rhoades', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Isabella', 'Jones', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Mia', 'Moore', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Lacy', 'Martinez', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Sonya', 'Garcia', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Mia', 'Taylor', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Abigail', 'Anne', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Kylie', 'Miller', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Sofia', 'Page', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Avery', 'Clark', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Ella', 'Lopez', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Scarlett', 'Lee', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Eva', 'Walker', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Chloe', 'Hall', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Victoria', 'Wright', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Riley', 'Martin', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Aria', 'Daniels', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Samantha', 'Turner', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Zoey', 'Harper', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Penelope', 'Young', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Addison', 'Scott', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Emily', 'King', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Nicole', 'Harris', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Hazel', 'Faith', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Stacy', 'Clark', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Emma', 'Green', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Stella', 'Davis', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Isabel', 'Martinez', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Lisa', 'Rodriguez', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Audrey', 'Parker', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Leah', 'Turner', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Mia', 'Mitchell', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Cory', 'Hall', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Brooklyn', 'Adams', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Alexis', 'Foster', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Lauren', 'Bennet', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Alexis', 'Reed', 'Female');
INSERT INTO actors (A_Name, A_LastName, A_Gender) VALUES ('Kennedy', 'Adams', 'Female');

INSERT INTO filmcast (F_ID, A_ID) VALUES ('1', '35');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('1', '71');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('1', '24');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('1', '32');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('1', '56');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('1', '67');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('1', '4');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('1', '58');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('2', '5');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('2', '36');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('2', '34');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('2', '58');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('2', '28');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('2', '72');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('2', '61');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('2', '33');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('3', '11');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('3', '7');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('3', '51');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('3', '52');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('3', '59');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('3', '69');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('4', '1');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('5', '3');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('5', '6');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('5', '52');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('6', '9');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('6', '17');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('6', '38');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('6', '62');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('6', '53');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('6', '14');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('6', '41');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('7', '71');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('7', '62');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('7', '52');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('7', '14');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('7', '16');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('7', '19');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('7', '51');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('8', '41');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('8', '35');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('8', '42');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('8', '31');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('8', '2');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('8', '18');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('8', '5');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('8', '52');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('9', '3');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('9', '27');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('9', '25');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('9', '21');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('9', '57');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('9', '67');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('9', '58');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('10', '22');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('10', '70');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('10', '50');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('10', '40');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('10', '20');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('10', '15');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('10', '4');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('10', '39');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('11', '70');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('11', '69');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('11', '68');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('11', '67');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('11', '66');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('11', '65');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('11', '1');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('12', '17');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('12', '20');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('12', '43');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('12', '47');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('12', '69');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('12', '61');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('12', '66');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('12', '21');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('13', '1');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('14', '6');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('14', '9');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('14', '38');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('14', '41');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('14', '44');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('14', '49');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('15', '50');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('15', '10');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('15', '60');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('15', '67');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('15', '51');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('15', '58');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('16', '1');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('17', '2');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('17', '3');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('17', '4');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('17', '5');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('17', '69');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('18', '2');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('18', '52');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('19', '17');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('19', '61');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('20', '5');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('20', '52');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('20', '60');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('21', '36');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('21', '38');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('21', '7');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('21', '18');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('22', '1');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('22', '34');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('23', '36');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('23', '39');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('23', '46');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('23', '49');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('23', '58');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('23', '62');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('24', '15');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('24', '70');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('25', '1');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('26', '72');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('26', '27');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('26', '61');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('26', '50');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('27', '16');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('27', '51');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('27', '40');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('28', '24');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('28', '14');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('28', '39');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('28', '47');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('29', '9');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('29', '47');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('29', '61');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('30', '25');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('30', '73');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('30', '48');
INSERT INTO filmcast (F_ID, A_ID) VALUES ('30', '2');
-- Remaining entries are not shown here for brevity



INSERT INTO users (U_Username, U_Password) VALUES ('admin', 'admin');
INSERT INTO users (U_Username, U_Password) VALUES ('bar', '2001');
